#! /bin/bash

cd ./x86_64

rm ArchCringeRepository*
repo-add ArchCringeRepository.db.tar.gz *.pkg.*

rm ArchCringeRepository.db ArchCringeRepository.files
mv ArchCringeRepository.db.tar.gz ArchCringeRepository.db
mv ArchCringeRepository.files.tar.gz ArchCringeRepository.files
